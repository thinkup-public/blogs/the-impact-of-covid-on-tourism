

Sabemos que la pandemia ha afectado a muchas areas, tanto para bien como para mal. Debido a las restricciones de aforo, aglomeraciones y distanciamiento, podríamos citar al turismo en general como una de las areas mas afectadas. Pero por otro lado, la necesidad de continuar haciendo funcionar los distintos negocios, ha hecho un efecto muy positivo en lo que a informática refiere.

Han sido muchos y muy variados los cambios que la tecnología ha aportado a la industria del turismo en estos casi 2 años de pandemia. Desde cosas simples como los menus digitales a través de códigos QR en las mesas, hasta la automatización de ingresos a hoteles sin contacto o las entradas digitales a edificios de interés.
No es que nada de esto no existiera previo a la situación actual, pero si que han dado un salto abismal y logrado llegar a gran parte de la industria. Resulta mas difícil hoy encontrar un hotel o un edificio donde tengas que hacer cola, pasar una tarjeta de crédito por la banda magnética o mucho menos pagar con efectivo. Estas situaciones han sido sustituidas por el self-checkin y el contactless, como ejemplos de evitar el contacto.
Si comparo mi ultima experiencia por Europa en 2019, donde he hecho la cola en casi todos los lugares a donde quise entrar, contra reciente del pasado mes de agosto, donde en algunos sitios debía comprar la entrada en una terminal apostada a la entrada o a través de una app con antelación, pasar por el molinete que se habilita con un QR y seguir las instrucciones para completar el recorrido. Que en mas de un lugar he pensado que no había gente allí (claro, hasta que un seguridad se me acercó para decirme que no podia tomar fotos).

Esto no solo ha generado un boom en la tecnología y un impacto en el turismo, si no que también a logrado agilizar los procesos para los turistas y alivianar la carga a quienes trabajan en estos sitios.

A pesar de haber sido cambios llevados a cabo con celeridad y por necesidad, es una transición que iba a ocurrir de todas maneras. 
En ese sentido, y como dice el dicho, “no hay mal que por bien no venga” y han venido para quedarse.
